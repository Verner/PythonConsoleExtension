General
========

For general guidelines see `Contributing to Open Source Projects <http://www.contribution-guide.org/>`_.

Setup Devel Environment
=========================

.. code-block:: shell-session

    # Clone the repo
    $ git clone https://gitlab.com/Verner/PythonConsoleExtension

    ...

    # Change to the project dir
    $ cd PythonConsoleExtension

    # Install dependencies
    $ yarn install

Then, if you are on Chrome/Chromium, open the `Extensions configuration page <chrome://extensions/>`_, enable
developer mode click the `Load unpacked extensions` button and select the src subdirectory in the dialog which
opens. Then navigate to a page, open the developer tools and select the `PythonConsole` tab. You can debug
the extension by right-clicking on the console tab and selecting 'Inspect'.


Code Layout
===========

- node_modules/           ... external dependencies
- src/                    ... main extension code
    - css/                ... stylesheets
    - img/                ... images
    - libs/               ... external library minified jsfiles (copied from node_modules)
    - main.html, main.js  ... the entry point into the extension (registers the `PythonConsole` tab with developer tools)
    - console.html        ... the html source for the console tab
    - console.js          ... runs brython when the console tab is loaded
    - console.py          ... the main extension code, written in Python, creates the console, manages running python code and printing out the results
    - message_hub.js, message_relay.js  ... provides for message passing from the inspected window to the console; it is somewhat convoluted due to the restrictions in place; to send a message, the inspected page emits a message on the window object, this is handled by an injected script (message_relay.js) and forwarded to a backround page (running message_hub.js) which only then forwards it to the console page
    - parse.py            ... a very simple parser used to determine whether the code which the user typed so far into the console is complete and ready to execute or whether we need more input (e.g. if the last line ends with a semicolon, or braces have not been terminated, ...)
    - utils.py            ... miscellaneous utility functions used in console.py
    - run_python_helper.js... function which uses Brython to execute python code in the context of the inspected page and returns the result

