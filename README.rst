Provides a Brython powered Python console to Web Inspector

Python Console
==============

A browser extension to provide a `Brython <https://www.brython.info>`_ powered Python console to the
Developer Tools (Web Inspector)


Bugs/Requests
-------------

Please use the `GitLab issue tracker <https://gitlab.com/Verner/PythonConsoleExtension/issues>`_ to submit bugs or request features.


Credits
-------

  - `Brython <https://www.brython.info>`_: (Browser Python) is an implementation of Python 3 running in the browser (BSD-3 Licensed)
  - `jq-console <http://replit.github.io/jq-console/>`_: A feature complete web terminal (MIT Licensed)

License
-------

Copyright (c) 2017 Jonathan L. Verner <jonathan@temno.eu>


Distributed under the terms of the BSD-3 license.

