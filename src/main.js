chrome.devtools.panels.create(
    "PythonConsole",
    "brython.png",
    "console.html",
    function cb(panel) {
        panel.onShown.addListener(function(win){ win.focus(); });
    }
);

