try {
(function ($B) {

    function run_script(script) {
        // script has attributes url, src, name
        $B.$py_module_path[script.name]=script.url
        var root, js
    $B.stderr.write = function(data){send_message({
        'type':'io',
        'stream':'stderr',
        'data':data
    })}
    $B.stdout.write = function(data){send_message({
        'type':'io',
        'stream':'stdout',
        'data':data
    })}



        try{
            // Conversion of Python source code to Javascript
            root = $B.py2js(atob(script.src),script.name,script.name,'__builtins__')
            js = root.to_js()
            // Run resulting Javascript
            eval(js)
            $B.imported[script.name] = $locals
            console.log($locals);
            if ($locals['_'] !== $B.builtins.None) {
                return {
                    'type':'object',
                    'value':$B.builtins.repr($locals['_'])
                }
            } else {
                return {
                    'type':'object',
                    'value':undefined
                }
            }
        }catch($err){
            if($B.debug>1){
                console.log($err)
                for(var attr in $err){
                console.log(attr+' : ', $err[attr])
                }
            }

            // If the error was not caught by the Python runtime, build an
            // instance of a Python exception
            if($err.$py_error===undefined){
                console.log('Javascript error', $err)
                $err=_b_.RuntimeError($err+'')
            }

            // Print the error traceback on the standard error stream
            var name = $err.__name__
            var $trace = _b_.getattr($err,'info')
            if(name=='SyntaxError' || name=='IndentationError'){
                var offset = $err.args[3]
                $trace += '\n    ' + ' '.repeat(offset) + '^' +
                    '\n' + name+': '+$err.args[0]

            }else{
                $trace += '\n'+name+': ' + $err.args
            }
            try{
                _b_.getattr($B.stderr,'write')($trace)
            }catch(print_exc_err){
                console.log($trace)
            }
            return {
                'type':'error',
                'value': $trace
            }
        }finally{
            root = null
            js = null
        }
    }

    var send_message = function(msg) {
        console.log("Sending message", msg);
        window.postMessage({
            content: msg,
            source: 'python-console'
        }, '*');
    }

    brython({debug:1})
    $B.run_dev_console_script = run_script
    send_message({
        'type':'status',
        'status':'ready'
    })
})(__BRYTHON__)
} catch(e) {}
