from browser import window
from javascript import jsobj2pyobj, pyobj2jsobj

def extern(func):
    """
        Wraps a python function into a function which can be called from JavaScript.

        Its arguments are wrapped in python objects via (`jsobj2pyobj`) and, if there
        are not enough of them, padded with `None` values (javascript allows not specifying
        all arguments and automatically replaces them with `undefined`). The return value
        is then converted into a javascript object using `pyobj2jsobj`.

        WARNING: Any exception thrown by the python code will be caught, logged into the
        console, but otherwise ignored (i.e. it won't be propagated)
    """

    def wrapped(*args):
        _args = []
        for a in args:
            try:
                _args.append(jsobj2pyobj(a))
            except:
                _args.append(a)
        if func.__code__.co_argcount > len(_args):
            _args.extend([None]*(func.__code__.co_argcount-len(_args)))
        try:
            ret = func(*_args)
            return pyobj2jsobj(ret)
        except Exception as ex:
            console.log("Exception ", ex, "while running ", func.__name__)
            return None
    return wrapped

def _encode_path(script_path):
    """
        Gets the absolute url of a file in the extension (reachable from the outside)
        and returns it together with a base64 encoded variant (using `window.btoa`)
    """
    url = window.chrome.runtime.getURL(script_path)
    id = window.btoa(url)
    return url, id

def inject_script(script_path, depends=[], force_reload=False):
    """
        Asynchronously (!) injects a script from the extension into an inspected page.
        The optionally :param:`depends` argument specifies what previously injected scripts
        need to finish loading before the currently injected script :param:`script_path`
        is loaded. If :param:`force_reload` is true, injects the script even if it was already
        injected before.
    """
    url, id = _encode_path(script_path)
    url = window.chrome.runtime.getURL(script_path)
    js = "var script_name = '"+script_path+"';\n"
    if force_reload:
        js += "var force_reload=true;\n"
    else:
        js += "var force_reload=false;\n"
    js += "var dependencies = ["+",".join(['"'+_encode_path(d)[1]+'"' for d in depends])+"];\n";
    js += """
if (window._injected === undefined) window._injected = [];
var id = '"""+id+"""';
var script = document.getElementById(id);
if (! script || force_reload) {
    if (script) document.body.removeChild(script);
    script=document.createElement('script');
    script.id = id;
    script.src = '"""+url+"""';
    window._injected['"""+id+"""'] = new Promise(function(resolve, reject) {
        script.onload = function () {
            window.postMessage({
                content: {
                    'type':'status',
                    'status':'loaded',
                    'script':'"""+script_path+"""'
                },
                source: 'python-console'
            }, '*');
            console.log("Loaded "+'"""+script_path+"""')
            resolve();
        }
    });
    var dep_promises = [];
    for(let dep of dependencies) {
        if (window._injected[dep] !== 'undefined') dep_promises.push(window._injected[dep])
    }
    Promise.all(dep_promises).then(function() {
        document.body.appendChild(script);
    })
} else {
    window.postMessage({
        content: {
            'type':'status',
            'status':'loaded',
            'script':'"""+script_path+"""'
        },
        source: 'python-console'
    }, '*');
}

"""
    window.chrome.devtools.inspectedWindow.eval(js);

