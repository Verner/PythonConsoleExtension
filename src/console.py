import sys

from browser import window, document, console, html

import parse
from utils import inject_script, extern

EXTENSION_ID = window.chrome.i18n.getMessage("@@extension_id")
EXTENSION_VERSION = window.chrome.runtime.getManifest().version


class Console:
    LAST_ID = -1
    INDENT = 4

    def __init__(self, elt):
        self._elt = elt
        if not self._elt.id:
            self.LAST_ID += 1
            self._elt.id = 'console-number-'+str(self.LAST_ID)

        self._console = window.jQ(self._elt).jqconsole('PythonConsole '+str(EXTENSION_VERSION), ">>> ", "... ")
        self._console.SetIndentWidth(self.INDENT)
        self._console.RegisterMatching('{', '}', 'braces')
        self._console.RegisterMatching('(', ')', 'parens')
        self._console.RegisterMatching('[', ']', 'brackets')

        self._hub = window.chrome.runtime.connect(EXTENSION_ID, {'name': "python-console"})
        self._hub.postMessage({
            'name': 'init',
            'tabId': window.chrome.devtools.inspectedWindow.tabId
        })
        self._hub.onMessage.addListener(self._on_message)
        info_msg = html.DIV()
        info_msg.Class = 'jq-console-system'
        info_msg.innerHTML = "<a target='_blank' href='https://www.brython.info'>Brython</a> {major}.{minor}.{patch} on {navigator} ({navigator_version})".format(
            major=sys.implementation.version[0],
            minor=sys.implementation.version[1],
            patch=sys.implementation.version[2],
            navigator=window.navigator.appName,
            navigator_version=window.navigator.appVersion
        )
        self._console.Append(info_msg)
        self._console.Write("\nInjecting scripts into page...\n", 'jq-console-system')
        inject_script('libs/brython.js')
        inject_script('libs/brython_stdlib.js')
        inject_script('run_python_helper.js', depends=['libs/brython.js', 'libs/brython_stdlib.js'])
        self._disabled_streams={}
        window.chrome.devtools.inspectedWindow.eval("""
            if(__BRYTHON__.run_dev_console_script) window.postMessage({
                content: {
                    type:'status',
                    'status': 'ready'
                },
                source: 'python-console'
        }, '*');
        """)

    @extern
    def _on_message(self, message, port):
        try:
            message=message.content
            tp = message.type
            if tp == 'io':
                data = str(message.data)
                stream = str(message.stream)
                if stream not in self._disabled_streams:
                    self._console.Write(data, 'jqconsole-'+stream)
            elif tp == 'status':
                if message.status == 'ready':
                    self._start_prompt()
                elif message.status == 'loaded':
                    self._console.Write(" "+message.script+"\n", 'jqconsole-system')
                    if message.script == 'run_python_helper.js':
                        self._start_prompt()
            else:
                console.log("Unhandled message", message)
        except Exception as ex:
            self._console.Write("Error when handling message: "+str(ex)+"\n", 'jqconsole-error')
        return True


    def _handle_input(self, input):
        self.run_code(input)

    def _start_prompt(self):
        self._console.Prompt(True, self._handle_input, self._multiline)

    @extern
    def _multiline(self, src):
        state, indent_level = parse.multiline(src, indent_width=self.INDENT)

        if state == parse.C_DONE:
            return False
        elif state == parse.C_BLOCK:
            return 1
        elif state == parse.C_UNINDENT:
            return -1
        elif state == parse.C_BACKSLASH:
            return 0
        elif state == parse.C_BR:
            return 1
        elif state == parse.C_STRING:
            return 0
        elif state == parse.C_SAMEINDENT:
            return 0
        return False

    @extern
    def _result_handler(self, result, isException):
        console.log("Result", result, isException)
        try:
            if isException:
                self._console.Write(str(result)+"\n", 'jqconsole-error')
            elif 'value' in result and result['value']:

                if result['type'] == 'error':
                    self._console.Write(str(result['value'])+"\n", 'jqconsole-error')
                    self._disabled_streams['stderr'] = True
                else:
                    self._console.Write(str(result['value'])+"\n", 'jqconsole-result')
        except Exception as ex:
            self._console.Write("Unknown error: "+str(ex)+"\n", 'jqconsole-error')
            console.log(result)
        self._start_prompt()

    def run_code(self, code):
        wrapped_code = '_=exec("""'+code.replace('"""', '\\"""')+'""")'
        js = """__BRYTHON__.run_dev_console_script({
            'url':'devtools_console',
            'name':'devtools_console',
            'src':'"""+window.btoa(wrapped_code)+"""'
        })"""
        self._disabled_streams = {}
        window.chrome.devtools.inspectedWindow.eval(js, self._result_handler)




hub = window.chrome.runtime.connect({
    'name': 'devtools-page'
})

document['loading'].style.display = 'none'
cons = Console(document['console'])
