if (window._message_relays === undefined) {
    window._message_relays = [];
}

var window_message_handler = function(event) {
  // Only accept messages from the same frame
  if (event.source !== window) {
    return;
  }

  var message = event.data;

  // Only accept messages that we know are ours
  if (typeof message !== 'object' || message === null ||
      !message.source === 'python-console') {
    return;
  }
  console.log("Relaying message from webpage", message)

  chrome.runtime.sendMessage(chrome.runtime.id, message);
}

for(i=0;i<window._message_relays.length;i++) {
    window.removeEventListener('message', window._message_relays[i])
}

window.addEventListener('message', window_message_handler);
window._message_relays.push(window_message_handler)
