var connections = {}; // keeps associations of pages (tabs) with
                      // their Devtool Python Consoles

chrome.runtime.onConnect.addListener(function (port) {
    // Called whenever some instance of a DevTools Python console
    // starts up and connects to the message hub (running in the background page)

    var extensionListener = function (message, sender, sendResponse) {
        // Listen to messages sent from the DevTools Python Console

        if (message.name == "init") {

            // Associate the inspected page (identified by message.tabId)
            // with its console (identified by the port);
            connections[message.tabId] = port;

            // Inject the message relay as a content script into
            // the inspected page (identified by message.tabId)
            chrome.tabs.executeScript(message.tabId,{
                file:'message_relay.js'
            })
          return;
        }
    }

    port.onMessage.addListener(extensionListener);

    port.onDisconnect.addListener(function(port) {
        // When the devtools disconnect, remove the
        // listeners so that we may eventually stop
        // running
        port.onMessage.removeListener(extensionListener);
        var tabs = Object.keys(connections);

        // Remove the associations of tabs to the devtools
        // inspecting them
        for (var i=0, len=tabs.length; i < len; i++) {
          if (connections[tabs[i]] == port) {
            delete connections[tabs[i]]
            break;
          }
        }
    });
});

// Receive message from content script and relay to the devTools page for the
// current tab
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    // Messages from content scripts should have sender.tab set
    if (sender.tab) {
      var tabId = sender.tab.id;
      if (tabId in connections) {
        console.log("Sending message to python-console for tab", tabId)
        console.log(" Message", request)
        connections[tabId].postMessage(request);
      } else {
        console.log("Tab not found in connection list.");
      }
    } else {
      console.log("sender.tab not defined.");
    }
    return true;
});
