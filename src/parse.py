S_STRING = 0
S_INDENT = 1
S_OTHER = 2

C_BACKSLASH = 0
C_BR = 1
C_STRING = 2
C_BLOCK = 3
C_DONE = 4
C_UNINDENT = 5
C_SAMEINDENT = 6


def count_indent(self, ln, indent_width=4):
        if not ln:
            return 0
        pos = 0
        sp = 0
        while(ln[pos] in [' ', '\t']):
            if ln[pos] == ' ':
                sp += 1
            else:
                sp += indent_width
            pos += 1
        return int(sp/indent_width)


def multiline(src, indent_width=4):
    br_stack = []

    br_open = ['[', '(', '{']
    br_close = [']', ')', '}']
    br_pairs = {'[': ']', '(': ')', '{': '}'}

    pos = 0
    length = len(src)
    status = S_INDENT
    string_type = None
    escape_next = False
    indent = 0
    error = False

    while(pos < length):
        ch = src[pos]

        if status == S_STRING:
            if escape_next:
                escape_next = False
            elif ch == '\\':
                escape_next = True
            elif src[pos:pos+len(string_type)] == string_type:
                status = S_OTHER
            elif len(string_type) == 1 and ch == '\n':
                error = True
                break
        elif status == S_INDENT:
            if ch == ' ':
                indent += 1
            else:
                status = S_OTHER
                pos -= 1  # need to read the char again
        elif status == S_OTHER:
            if escape_next:
                escape_next = False
            elif ch in ['"', "'"]:
                status = S_STRING
                if ch == '"' and src[pos:pos+3] == '"""':
                    string_type = '"""'
                    pos += 2
                else:
                    string_type = ch
            elif ch == '\n':
                status = S_INDENT
                indent = 0
            elif ch == '\\':
                escape_next = True
            elif ch in br_open:
                br_stack.append((ch, int(indent/indent_width)+1))
            elif ch in br_close:
                if not br_stack or not br_pairs[br_stack[-1][0]] == ch:
                    error = True
                    break
                del br_stack[-1]
        pos += 1

    if error:
        # Fail early
        return (C_DONE, 0)

    if status == S_STRING:
        if len(string_type) > 1:
            # This is an error, fail early
            return (C_DONE, 0)
        else:
            return (C_STRING, 0)
    elif status == S_INDENT:
        if indent < indent_width:
            return (C_UNINDENT, int(indent/indent_width)-1)
        else:
            return (C_DONE, 0)
    elif status == S_OTHER:
        if br_stack:
            return (C_BR, br_stack[-1][1])
        elif escape_next:
            return (C_BACKSLASH, int(indent/indent_width))
        elif src.strip().endswith(':'):
            return (C_BLOCK, int(indent/indent_width)+1)
        elif indent >= indent_width:
            return (C_SAMEINDENT, 0)
    return (C_DONE, 0)
